package fr.messaging.server;

import java.net.SocketException;

public class MainAuthServer {
    public static void main(String[] args) throws SocketException {
        ServerAuth serverAuth = new ServerAuth();
        serverAuth.start();
    }
}
