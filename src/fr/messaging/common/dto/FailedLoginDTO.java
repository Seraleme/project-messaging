package fr.messaging.common.dto;

import java.io.Serializable;

public class FailedLoginDTO implements Serializable {
    private String error;

    public FailedLoginDTO(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
