package fr.messaging.client.service;

import fr.messaging.client.socket.NodeServer;
import fr.messaging.common.dto.MessageDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NodeService {

    private final int id;
    private int sendSeq = 0;
    private final int[] delivered;
    private final List<MessageDTO> buffer = new ArrayList<>();
    private final NodeServer nodeServer;

    public NodeService(int id) throws Exception {
        this.id = id;
        this.delivered = new int[10];
        this.nodeServer = new NodeServer(this);
    }

    public void createMessage(String msg) throws Exception {
        if (msg.matches("\\d+-.*")) {
            final String[] split = msg.split("-", 2);
            final int receiverId = Integer.parseInt(split[0]);

            MessageDTO messageDTO = new MessageDTO(this.id, split[1].trim(), null);
            this.nodeServer.sendMessage(receiverId, messageDTO);
            System.out.print("\r[Private] to " + receiverId + ": " + messageDTO.getMessage() + "\n");
        } else {
            MessageDTO messageDTO = new MessageDTO(this.id, msg, this.delivered);
            this.delivered[this.id] = ++this.sendSeq;
            this.nodeServer.broadcastMessage(messageDTO);
            System.out.print("\r[All] " + messageDTO.getSenderId() + ": " + messageDTO.getMessage() + "\n");
        }
    }

    public void receiveMessage(MessageDTO messageDTO) {
        buffer.add(messageDTO);
        final List<MessageDTO> list = new ArrayList<>(this.buffer);
        for (MessageDTO message : list) {
            if (isInTimeStampOrder(message) && isMissingMessage(message.getDeps())) {
                deliverMessage(message);
                this.delivered[message.getSenderId()] = this.delivered[message.getSenderId()] + 1;
                buffer.remove(message);
            }
        }
    }

    public void auth(String password) throws IOException, ClassNotFoundException {
        nodeServer.auth(id, password);
    }

    public boolean isInTimeStampOrder(MessageDTO messageDTO) {
        return this.delivered[messageDTO.getSenderId()] + 1 == messageDTO.getDeps()[messageDTO.getSenderId()];
    }

    public boolean isMissingMessage(int[] deps) {
        for (int i = 0; i < deps.length; i++) {
            if (this.delivered[i] > deps[i])
                return false;
        }
        return true;
    }

    public void deliverMessage(MessageDTO messageDTO) {
        if (messageDTO.getDeps() == null)
            System.out.print("\r[Private] from " + messageDTO.getSenderId() + ": " + messageDTO.getMessage() + "\n> ");
        else
            System.out.print("\r[All] " + messageDTO.getSenderId() + ": " + messageDTO.getMessage() + "\n> ");
    }

    public int getId() {
        return id;
    }

    public void stop() {
        nodeServer.stopRunning();
    }
}
