package fr.messaging.client.socket;

import fr.messaging.client.MainNode;
import fr.messaging.client.service.NodeService;
import fr.messaging.common.dto.*;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.CompletableFuture;

public class NodeServer extends Thread {

    private static final String CLIENT_SERVER_ADDRESS = "localhost";
    private static final String AUTH_SERVER_ADDRESS = "localhost";
    private static final int AUTH_SERVER_PORT = 4445;

    private final DatagramSocket socket;
    private boolean running;
    private final byte[] buf = new byte[6400];
    private NodeDTO[] nodes;
    private final NodeService nodeService;

    public NodeServer(NodeService nodeService) throws Exception {
        socket = new DatagramSocket(0, InetAddress.getByName(CLIENT_SERVER_ADDRESS));
        this.nodeService = nodeService;
        start();
    }

    public void run() {
        running = true;
        while (running) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                if (!e.getMessage().contains("Socket closed"))
                    throw new RuntimeException(e);
            }

            try {
                final ByteArrayInputStream bais = new ByteArrayInputStream(buf);
                final ObjectInputStream ois = new ObjectInputStream(bais);
                Object received = ois.readObject();

                if (received instanceof MessageDTO) {
                    final MessageDTO messageDTO = (MessageDTO) received;
                    if (messageDTO.getDeps() == null)
                        nodeService.deliverMessage(messageDTO);
                    else
                        nodeService.receiveMessage(messageDTO);
                } else if (received instanceof NodesDTO) {
                    final NodesDTO nodesDTO = (NodesDTO) received;
                    if (nodesDTO.getNodes() != null) {
                        this.nodes = nodesDTO.getNodes();
                        CompletableFuture.runAsync(MainNode::chat);
                    }
                } else if (received instanceof FailedLoginDTO) {
                    CompletableFuture.runAsync(MainNode::login);
                } else if (received instanceof NodeDTO) {
                    final NodeDTO nodesDTO = (NodeDTO) received;
                    this.nodes[nodesDTO.getId()] = nodesDTO;
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (!socket.isClosed())
            socket.close();
    }

    public void stopRunning() {
        this.running = false;
        socket.close();
    }

    public void auth(int id, String password) throws IOException {
        sendObject(new AuthentificationDTO(id, password), InetAddress.getByName(AUTH_SERVER_ADDRESS), AUTH_SERVER_PORT);
    }

    public void sendMessage(int nodeId, MessageDTO messageDTO) throws Exception {
        sendObjectToNode(nodeId, messageDTO);
    }

    public void broadcastMessage(MessageDTO messageDTO) throws Exception {
        if (nodes == null)
            throw new Exception("No nodes available");
        for (NodeDTO node : nodes)
            if (node != null && node.getId() != this.nodeService.getId())
                sendObjectToNode(node.getId(), messageDTO);
    }

    public void sendObjectToNode(int nodeId, Object object) throws Exception {
        if (nodes == null)
            throw new Exception("No nodes available");
        if (nodes[nodeId] == null)
            throw new Exception("Node " + nodeId + " not available");
        sendObject(object, InetAddress.getByAddress(nodes[nodeId].getAddress()), nodes[nodeId].getPort());
    }

    public void sendObject(Object object, InetAddress inetAddress, int port) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(6400);
        final ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        final byte[] data = baos.toByteArray();

        socket.send(new DatagramPacket(data, data.length, inetAddress, port));
    }
}
