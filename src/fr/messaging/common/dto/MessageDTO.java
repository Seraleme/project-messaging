package fr.messaging.common.dto;

import java.io.Serializable;

public class MessageDTO implements Serializable {
    private int senderId;
    private String message;
    private int[] deps;

    public MessageDTO(int senderId, String message, int[] deps) {
        this.senderId = senderId;
        this.message = message;
        this.deps = deps;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int[] getDeps() {
        return deps;
    }

    public void setDeps(int[] deps) {
        this.deps = deps;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }
}
