package fr.messaging.server;

import fr.messaging.common.dto.AuthentificationDTO;
import fr.messaging.common.dto.FailedLoginDTO;
import fr.messaging.common.dto.NodeDTO;
import fr.messaging.common.dto.NodesDTO;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ServerAuth extends Thread {
    private static final int AUTH_SERVER_PORT = 4445;

    private final DatagramSocket socket;
    private boolean running;
    private final byte[] buf = new byte[6400];

    private final NodeDTO[] nodes;


    public ServerAuth() throws SocketException {
        this.socket = new DatagramSocket(AUTH_SERVER_PORT);
        this.nodes = new NodeDTO[10];
    }


    public void run() {
        running = true;

        while (running) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // Received
            AuthentificationDTO received;
            try {
                final ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
                final ObjectInputStream ois = new ObjectInputStream(bais);
                received = (AuthentificationDTO) ois.readObject();

                final ByteArrayOutputStream baos = new ByteArrayOutputStream(6400);
                final ObjectOutputStream oos = new ObjectOutputStream(baos);

                if ("azerty".equals(received.getPassword())) {
                    final NodeDTO nodeDTO = new NodeDTO(received.getId(), packet.getAddress().getAddress(), packet.getPort());
                    broadcast(nodeDTO);
                    this.nodes[received.getId()] = nodeDTO;

                    System.out.println("Authentification of node " + received.getId() + ", with ip: " + packet.getAddress().getHostAddress() + ":" + packet.getPort());

                    oos.writeObject(new NodesDTO(nodes));
                    final byte[] data = baos.toByteArray();
                    packet = new DatagramPacket(data, data.length, packet.getAddress(), packet.getPort());
                    socket.send(packet);
                } else {
                    oos.writeObject(new FailedLoginDTO("Invalid password"));
                    final byte[] data = baos.toByteArray();
                    packet = new DatagramPacket(data, data.length, packet.getAddress(), packet.getPort());
                    socket.send(packet);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        socket.close();
    }

    public void broadcast(Object object) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(6400);
        final ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        final byte[] data = baos.toByteArray();

        for (NodeDTO node : nodes) {
            if (node != null) {
                try {
                    DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByAddress(node.getAddress()), node.getPort());
                    socket.send(packet);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        }
    }

}
