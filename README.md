# Project Messaging

## Information

Run a **Node** by running `fr.messaging.client.MainNode.main()`.
Run the **AuthServer** by running `fr.messaging.server.MainAuthServer.main()`.

Or you can find in `out/artifacts` the **`node.jar`** and the **`authserver.jar`** 