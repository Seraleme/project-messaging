package fr.messaging.common.dto;

import java.io.Serializable;

public class NodesDTO implements Serializable {
    private NodeDTO[] nodes;

    public NodesDTO(NodeDTO[] nodes) {
        this.nodes = nodes;
    }

    public NodeDTO[] getNodes() {
        return nodes;
    }

    public void setNodes(NodeDTO[] nodes) {
        this.nodes = nodes;
    }
}
