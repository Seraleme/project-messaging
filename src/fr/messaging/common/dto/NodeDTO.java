package fr.messaging.common.dto;

import java.io.Serializable;

public class NodeDTO implements Serializable {
    private int id;
    private byte[] address;
    private int port;

    public NodeDTO(int id, byte[] address, int port) {
        this.id = id;
        this.address = address;
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getAddress() {
        return address;
    }

    public void setAddress(byte[] address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
