package fr.messaging.client;

import fr.messaging.client.service.NodeService;

import java.util.Scanner;

public class MainNode {

    private static NodeService nodeService;
    private static Scanner scanner;

    public static void main(String[] args) throws Exception {
        scanner = new Scanner(System.in);
        System.out.print("Enter the node id\n> ");
        int nodeId;
        do {
            while (!scanner.hasNextInt()) scanner.next();
            nodeId = scanner.nextInt();
        } while (nodeId < 0 || nodeId > 9);

        nodeService = new NodeService(nodeId);
        scanner.nextLine();
        login();
    }

    public static void login() {
        System.out.print("Enter the password\n");
        String password;
        do {
            System.out.print("> ");
            password = scanner.nextLine();
        } while (password == null || password.isEmpty());
        try {
            nodeService.auth(password);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static void chat() {
        System.out.println("\n\n=====================================================================\n");
        System.out.println("Welcome Node " + nodeService.getId() + " to the chat !\n");
        System.out.println("Write global message in the chat or");
        System.out.println("Write private message to a node using format \"<id>-<message>\" like \"1-hy !\"\n");
        System.out.println("Write\"end\" to quit.");
        System.out.println("\n=====================================================================");

        try {
            String message;
            do {
                System.out.print("> ");
                message = scanner.nextLine();
                if (!"end".equals(message))
                    nodeService.createMessage(message);
            } while (!"end".equals(message));
            scanner.close();
            nodeService.stop();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
